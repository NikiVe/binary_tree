<?php

use DataStructure\Node;
use DataStructure\Tree;

include_once __DIR__ . '/vendor/autoload.php';
$N = 10;

if ($N > 256) {
    ini_set('xdebug.max_nesting_level', $N);
}

$tree = new Tree();

$sT = microtime(true);
for($i = 1; $i <= $N; $i++) {
    $t = random_int($i, $i * 10);
    $arValues[] = $t;
    $tree->insert($t);
}
echo 'Время добавления time: ' . round(microtime(true) - $sT, 4) . PHP_EOL;

$sT = microtime(true);
for($i = 0; $i < $N / 10; $i++) {
    $t = $arValues[random_int(0, $N - 1)];
    $tree->search($t);
}
echo 'Время поиска time: ' . round(microtime(true) - $sT, 4) . PHP_EOL;

$sT = microtime(true);
for($i = 0; $i < $N / 10; $i++) {
    $t = $arValues[random_int(0, $N - 1)];
    $tree->remove($t);
}
echo 'Время удаления time: ' . round(microtime(true) - $sT, 4) . PHP_EOL;