<?php


namespace DataStructure;


class Node
{
    protected Tree $tree;

    protected ?Node $l = null;
    protected ?Node $r = null;
    protected ?Node $parrent = null;

    protected $key = null;

    public function __construct($x) {
            $this->key = $x;
    }

    public function insert(Node $node): bool {
        if ($node->key < $this->key) {
            if ($this->l === null) {
                $this->addLeft($node);
                return true;
            }

            return $this->l->insert($node);
        }

        if ($node->key > $this->key) {
            if ($this->r === null) {
                $this->addRight($node);
                return true;
            }

            return $this->r->insert($node);
        }

        return false;
    }

    public function search(int $key) {
        if ($this->key === $key) {
            return $this;
        }
        if ($this->key === null) {
            return false;
        }

        if ($key < $this->key) {
            if ($this->l === null) {
                return false;
            }

            return $this->l->search($key);
        }

        if ($key > $this->key) {
            if ($this->r === null) {
                return false;
            }

            return $this->r->search($key);
        }
        return false;
    }

    public function detach(Node $node) {
        if ($this->l === $node) {
            $this->l->unsetParent();
            $this->l = null;
        }
        if ($this->r === $node) {
            $this->r->unsetParent();
            $this->r = null;
        }
    }

    public function detachChildren() {
        if ($this->l) {
            $this->detach($this->l);
        }
        if ($this->r) {
            $this->detach($this->r);
        }
    }

    public function getKey() {
        return $this->key;
    }

    public function getParrent() {
        return $this->parrent;
    }

    public function setParrent(Node $node) {
        $this->parrent = $node;
        return $this;
    }

    public function unsetParent() {
        $this->parrent = null;
    }

    public function getLeft() {
        return $this->l;
    }

    public function setLeft(Node $node) {
        return $this->l = $node;
    }

    public function getRight() {
        return $this->r;
    }

    public function setRight(Node $node) {
        return $this->r = $node;
    }

    public function addLeft(Node $node): Node {
        $this->l = $node;
        $this->l->setParrent($this);
        return $this;
    }

    public function addRight(Node $node) : Node {
        $this->r = $node;
        $this->r->setParrent($this);
        return $this;
    }

    public function setTree(Tree $tree) {
        $this->tree = $tree;
        return $this;
    }

    public function getTree() {
        return $this->tree;
    }

    public function isLeft() {
        if ($this->parrent->l === $this) {
            return true;
        }
        if ($this->parrent->r === $this) {
            return false;
        }
        throw new \RuntimeException('oooops');
    }

    public function isRight() {
        return !$this->isLeft();
    }

    public function printAll() {
        if ($this->l) {
            $this->l->printAll();
        }
        echo $this->key . ' ';
        if ($this->r) {
            $this->r->printAll();
        }
    }
}