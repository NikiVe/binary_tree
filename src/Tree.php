<?php


namespace DataStructure;


class Tree
{
    protected NodeV2 $node;

    public function insert(int $key) {
        $node = (new NodeV2($key))->setTree($this);
        if (!isset($this->node)) {
            $this->node = $node;
            return true;
        }
        return $this->node->insert($node);

    }

    public function remove(int $key) {
        $newRoot = null;
        if ($node = $this->node->search($key)) {
            if ($node->getParrent() === null) {
                $lNode = $node->getLeft();
                $rNode = $node->getRight();

                if ($lNode) {
                    $node->detachChildren();
                    if ($rcNode = $lNode->getRight()) {
                        $lNode->detach($rcNode);
                    }
                    if ($rNode) {
                        $lNode->insert($rNode);
                    }
                    if ($rcNode) {
                        $lNode->insert($rcNode);
                    }
                    $newRoot = $lNode;
                } elseif ($rNode) {
                    $node->detachChildren();
                    $newRoot = $rNode;
                }
            } else {
                if ($parent = $node->getParrent()) {
                    if ($node->isLeft()) {
                        $parent->detach($node);
                        if ($node->getLeft() !== null) {
                            $parent->setLeft($node->getLeft());
                        }
                        if ($node->getRight() !== null) {
                            $parent->insert($node->getRight());
                        }
                    } else {
                        $parent->detach($node);
                        if ($node->getLeft() !== null) {
                            if ($rNode = $node->getRight()) {
                                $parent->setRight($rNode);
                            }
                        }
                        if ($lNode = $node->getLeft()) {
                            $parent->insert($lNode);
                        }
                    }
                    unset($node);
                }
            }
        }
        if ($newRoot) {
            $this->node = $newRoot;
        }
    }

    public function search(int $key) {
        return $this->node->search($key);
    }

    public function getNode() : NodeV2 {
        return $this->node;
    }

    public function setRootNode(NodeV2 $node) {
        $this->node = $node;
    }
}