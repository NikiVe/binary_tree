<?php


namespace DataStructure;


use PhpParser\Builder\Param;

class NodeV2 extends Node
{
    protected $floor = 1;

    public function smallRightRotation() {
        if ($this->parrent) {
            // Открепить отца у его отца и сохранить в переменной
            $pNode = $this->parrent;
            if (isset($this->parrent->parrent)) {
                $ppNode = $this->parrent->parrent;
                $ppNode->detach($pNode);
            }

            // Открепить текущий элемент от отца
            $this->parrent->detach($this);

            // Если есть правый элемент, то сохранить ссылку на него и открепить его
            if ($rNode = $this->r) {
                $this->detach($this->r);
            }

            // Вставить отца
            $this->insert($pNode);
            // Если отец был Рутом, то стать вместо него рутом
            if ($this->tree->getNode() === $pNode) {
                $this->tree->setRootNode($this);
            }
            // Если был правый элемент, то его вставить в новый правый элемент
            if (isset($rNode)){

                $this->r->insert($rNode);
            }

            // Вставить нас в прадеда
            if (isset($ppNode)) {
                $ppNode->insert($this);
            }
            return $this;
        }
        throw new \RuntimeException('Невозможно выполнить правый поворот');
    }

    public function smallLeftRotation() {
        if ($this->parrent) {
            $pNode = $this->parrent;

            if (isset($this->parrent->parrent)) {
                $ppNode = $this->parrent->parrent;
                $ppNode->detach($pNode);
            }
            $this->parrent->detach($this);

            if ($lNode = $this->l) {
                $this->detach($this->l);
            }

            // Если отец был Рутом, то стать вместо него рутом
            if ($this->tree->getNode() === $pNode) {
                $this->tree->setRootNode($this);
            }
            $this->insert($pNode);
            if (isset($lNode)) {
                $this->l->insert($lNode);
            }

            if (isset($ppNode)) {
                $ppNode->insert($this);
            }
            return $this;
        }
        throw new \RuntimeException('Невозможно выполнить левый поворот');
    }

    public function detach(Node $node) {
        if ($this->l === $node) {
            $this->l->unsetParent();
            $this->l = null;
            if ($this->r) {
                $this->floor = $this->r->getFloor() + 1;
            }
        }
        if ($this->r === $node) {
            $this->r->unsetParent();
            $this->r = null;
            if ($this->l) {
                $this->floor = $this->l->getFloor() + 1;
            }
        }
    }

    public function bigRightRotation() {
        if ($this->parrent && $this->r) {
            $rNode = $this->r;
            $rNode->smallLeftRotation();
            $rNode->smallRightRotation();
            return $rNode;
        }
        throw new \RuntimeException('Невозможно выполнить Большой правый поворот');
    }

    public function bigLeftRotation() {
        if ($this->parrent && $this->l) {
            $rNode = $this->l;
            $rNode->smallRightRotation();
            $rNode->smallLeftRotation();
            return $rNode;
        }

        throw new \RuntimeException('Невозможно выполнить Большой левый поворот');
    }

    public function setRight(Node $node) {
        return $this->r = $node;
    }

    protected function updateFloor() {
        if ($this->l && $this->r) {
            $this->floor = $this->l->getFloor() >= $this->r->getFloor()
                ? $this->l->getFloor() : $this->r->getFloor();
            $this->floor++;
        } elseif ($this->l) {
            $this->floor = $this->l->getFloor() + 1;
        } else {
            $this->floor = $this->r->getFloor() + 1;
        }

        if ($parent = $this->getParrent()) {
            $parent->updateFloor();
        }
    }

    public function addLeft(Node $node): Node {
        $this->l = $node;
        $this->l->setParrent($this);
        if ($this->r === null) {
            $this->updateFloor();
        }
        return $this;
    }

    public function addRight(Node $node) : Node {
        $this->r = $node;
        $this->r->setParrent($this);
        if ($this->l === null) {
            $this->updateFloor();
        }
        return $this;
    }

    public function getFloor() {
        return $this->floor;
    }

    public function rebalance() {
        if ($this->isLeft()) {
            if ($this->l && $this->r && $this->l->getFloor() < $this->r->getFloor()) {
                $this->bigRightRotation();
            }
            $this->smallRightRotation();
        } else {
            if ($this->l && $this->r && $this->r->getFloor() < $this->l->getFloor()) {
                $this->bigLeftRotation();
            }
            $this->smallLeftRotation();
        }
    }
}